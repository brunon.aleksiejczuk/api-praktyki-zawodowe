import requests
from pprint import pprint
#from requests.api import request

class NewsAPI:
    def __init__(self):
        self.apiKey = '&apiKey=d3fddfef71914898affb1d0b9283ef08'
        NewsAPI.makeMenu(self)

    def makeMenu(self):
        
        print('1. Keyword')
        print('2. Top headlines\n')
        self.menuresponse = int(input(''))
        if self.menuresponse == 1:
            NewsAPI.endpointEverything(self)
        elif self.menuresponse == 2:
            NewsAPI.endpointHeadlines(self)
        
    def endpointEverything(self):
        #https://newsapi.org/v2/everything?q=bitcoin&apiKey=d3fddfef71914898affb1d0b9283ef08

        self.keyword = str(input('Keyword: '))
        self.base_url = "https://newsapi.org/v2/everything" +"?q=" + self.keyword + self.apiKey
        self.request = requests.get(self.base_url).json()
        pprint(self.request)
        

    def endpointHeadlines(self):
        #https://newsapi.org/v2/top-headlines?country=us&apiKey=d3fddfef71914898affb1d0b9283ef08

        self.country = 'country=' + str(input('Country (2 characters): '))
        self.base_url = "https://newsapi.org/v2/top-headlines?" + self.country + self.apiKey
        self.request = requests.get(self.base_url).json()
        pprint(self.request)

NewsAPI()